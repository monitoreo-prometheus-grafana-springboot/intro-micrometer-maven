package com.dev4j.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

public class CounterUsage {

    public static void main(String[] args) {
        MeterRegistry meterRegistry = new SimpleMeterRegistry();

        Counter counter = Counter.builder("dev4j.students")
                .description("Número de estudiantes de dev4j")
                .tag("course", "Métricas con Micrometer")
                .register(meterRegistry);

        counter.increment();
        counter.increment(200);

        System.out.println(counter.count());
    }
}
