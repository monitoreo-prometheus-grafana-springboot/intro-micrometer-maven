package com.dev4j.metrics;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import java.util.concurrent.TimeUnit;

public class TimerUsage {

    public static void main(String[] args) {
        MeterRegistry registry = new SimpleMeterRegistry();

        Timer timer = registry.timer("execution.time");
        timer.record(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println(i);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        });

        System.out.println(timer.totalTime(TimeUnit.SECONDS));
    }
}
