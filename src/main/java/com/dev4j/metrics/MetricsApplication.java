package com.dev4j.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Meter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import io.micrometer.core.instrument.config.NamingConvention;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

public class MetricsApplication {

    public static void main(String[] args) {

        CompositeMeterRegistry compositeMeterRegistry = Metrics.globalRegistry;

        Counter counter = compositeMeterRegistry.counter("numero.empleados", "oficina", "Benito Juares");

        MeterRegistry registry = new SimpleMeterRegistry();
        compositeMeterRegistry.add(registry);
        counter.increment();
        counter.increment(200);

        System.out.printf("Número de empleados %f", counter.count());


    }

//    private static void foo() {
//
//        CompositeMeterRegistry globalRegistry = Metrics.globalRegistry;
//
//        // Se puede implementar convencion de nombres
////        globalRegistry.config().namingConvention(new NamingConvention() {
////            @Override
////            public String name(String name, Meter.Type type, String baseUnit) {
////                return null;
////            }
////        });
//
//        Counter counter = globalRegistry.counter("numero.empleados", "oficina", "Benito Juares");
//        counter.increment(150);
//        System.out.printf("\nNúmero de empleados %f\n", counter.count());
//
//    }

}
