package com.dev4j.metrics;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GaugeUsage {

    public static void main(String[] args) {

        MeterRegistry registry = new SimpleMeterRegistry();

        List<String> list = new ArrayList<>(4);

        Gauge gauge = Gauge.builder("list.size", list, List::size)
                .register(registry);

        System.out.println(gauge.value());

        list.addAll(Arrays.asList("Devs4j", "raidentrance", "HolaMundo"));
        System.out.println(gauge.value());

    }
}
